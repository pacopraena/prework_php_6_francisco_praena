<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'/>
        <title>Formulario con PHP</title>
    </head>
    <body>
        <h1>Formulario con PHP</h1>

        <form action="comprobar.php" method="POST">
            <p>
                <label for="email">Email</label>
                <input type="email" name="email" required/>
            </p>
            
            <p>
                <label for="password">Contraseña</label>
                <input type="text" name="password" required/>
            </p>

            <p>
                <label for="password_check">Confirme contraseña</label>
                <input type="text" name="password_check" required/>
            </p>

            <input type="submit" value="Enviar"/>
        </form>

    </body>
</html>